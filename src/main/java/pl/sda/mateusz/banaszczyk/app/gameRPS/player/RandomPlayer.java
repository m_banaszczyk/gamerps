package pl.sda.mateusz.banaszczyk.app.gameRPS.player;

import pl.sda.mateusz.banaszczyk.app.gameRPS.GameAction;
import java.util.Random;

public class RandomPlayer extends Player {
    private Random random = new Random();

    public String nick;

    public RandomPlayer(String nick) {
        this.nick = nick;
    }

    public GameAction chooseAction() {
        int value = random.nextInt(3);
        int randomValue = random.nextInt() % 5;
//        return getGameActionIf(value);
        return getGameActionSwitch(value);
    }
    private GameAction getGameActionSwitch(int value){
        switch (value) {
            case 0:
                return GameAction.SCISSORS;
            case 1:
                return GameAction.ROCK;
            case 2:
                return GameAction.PAPER;
            default:
                throw new IllegalStateException();
        }
    }

//    private GameAction getGameActionIf(int value) {
//        if (value == 0) {
//            return GameAction.SCISSORS;
//        } else if (value == 1) {
//            return GameAction.ROCK;
//        } else {
//            return GameAction.PAPER;
//
//        }
//    }
}
