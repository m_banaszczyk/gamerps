package pl.sda.mateusz.banaszczyk.app.gameRPS.player;

import pl.sda.mateusz.banaszczyk.app.gameRPS.GameAction;
import java.util.Scanner;
import static pl.sda.mateusz.banaszczyk.app.gameRPS.player.GameRockPaperScissors.translation;

public class ScannerPlayer extends Player {

    public static String nick2;


    public ScannerPlayer(String nick2) {
        this.nick2 = nick2;
    }

    public String chooseNick() {
        Scanner nick1 = new Scanner(System.in);
        System.out.println(translation.nick());
        nick2 = nick1.next();
        return nick2;
    }

    public GameAction chooseAction() {
        Scanner scanner = new Scanner(System.in);

        System.out.println(translation.action());
        String input;
        do {
            input = scanner.next();

            switch (input.toLowerCase()) {
                case "k":
                case "r":
                case "rock":
                case "kamień":
                    return GameAction.ROCK;
                case "p":
                case "paper":
                case "papier":
                    return GameAction.PAPER;
                case "n":
                case "s":
                case "scissors":
                case "nożyce":
                    return GameAction.SCISSORS;
                default:
                    System.out.println("Wpisz poprawną wartość! / Wrong input!");
            }
        } while (true);
    }
}