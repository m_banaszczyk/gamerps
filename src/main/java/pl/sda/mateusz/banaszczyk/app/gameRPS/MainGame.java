package pl.sda.mateusz.banaszczyk.app.gameRPS;


import pl.sda.mateusz.banaszczyk.app.gameRPS.language.Translation;
import pl.sda.mateusz.banaszczyk.app.gameRPS.player.GameRockPaperScissors;
import pl.sda.mateusz.banaszczyk.app.gameRPS.player.RandomPlayer;
import pl.sda.mateusz.banaszczyk.app.gameRPS.player.ScannerPlayer;


import static pl.sda.mateusz.banaszczyk.app.gameRPS.player.ScannerPlayer.nick2;

public class MainGame {

    public static void main(String[] args) {


        RandomPlayer player1 = new RandomPlayer("Computer");
        ScannerPlayer player2 = new ScannerPlayer(nick2);

        GameRockPaperScissors game = new GameRockPaperScissors(player1, player2, Translation.chooseLanguage());

        game.startGame();
    }
}