package pl.sda.mateusz.banaszczyk.app.gameRPS.language;

import pl.sda.mateusz.banaszczyk.app.gameRPS.GameAction;
import pl.sda.mateusz.banaszczyk.app.gameRPS.player.RandomPlayer;
import pl.sda.mateusz.banaszczyk.app.gameRPS.player.ScannerPlayer;

import java.util.Scanner;


public abstract class Translation {

    public static String lang;

    public abstract String cont();

    public abstract String nick();

    public abstract String action();

    public abstract String actionText1(GameAction action1, RandomPlayer player1);

    public abstract String actionText2(GameAction action2, ScannerPlayer player2);

    public abstract String player1Win(RandomPlayer player1);

    public abstract String player2Win(ScannerPlayer player2);

    public abstract String draw();

    public static Translation chooseLanguage() {
        Scanner scanner = new Scanner(System.in);

        do {
            System.out.println("Wybierz język programu PL/EN:\nChoose your language PL/EN: p/e");
            lang = scanner.next();
            if (lang.equals("p")) {
                Translation translation = new PolishTranslation();
                return translation;
            } else if (lang.equals("e")) {
                Translation translation = new EnglishTranslation();
                return translation;
            } else {
                System.out.println("Podaj poprawną wartość!\nPlease enter a valid value!");
            }
        } while (!lang.equals("p") || !lang.equals("e"));
        return null;
    }

}

