package pl.sda.mateusz.banaszczyk.app.gameRPS.language;

import pl.sda.mateusz.banaszczyk.app.gameRPS.GameAction;
import pl.sda.mateusz.banaszczyk.app.gameRPS.player.ScannerPlayer;
import pl.sda.mateusz.banaszczyk.app.gameRPS.player.RandomPlayer;

public class PolishTranslation extends Translation {

    @Override
    public String cont() {
        return "Grasz dalej? t/n";
    }


    @Override
    public String action() {
        return "Wybierz akcję papier/kamień/nożyce: k/p/n";
    }

    @Override
    public String nick() {
        return "Podaj swój nick:";
    }

    @Override
    public String actionText1(GameAction action1, RandomPlayer player1) {
        return String.format("%s wykonał akcję 1: %s", player1.nick, getAction(action1));
    }

    @Override
    public String actionText2(GameAction action2, ScannerPlayer player2) {
        return String.format("%s wykonał akcję 2: %s", player2.nick2, getAction(action2));
    }

    @Override
    public String player1Win(RandomPlayer player1) {
        return player1.nick + " Zwycięstwo!";
    }

    @Override
    public String player2Win(ScannerPlayer player2) {
        return player2.nick2 + " ZWYCIĘSTWO!";
    }

    @Override
    public String draw() {
        return "Remis";
    }

    String getAction(GameAction action) {
        switch (action) {
            default:
            case SCISSORS:
                return "Nożyce";
            case PAPER:
                return "Papier";
            case ROCK:
                return "Kamień";
        }
    }
}
