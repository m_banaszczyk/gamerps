package pl.sda.mateusz.banaszczyk.app.gameRPS.language;

import pl.sda.mateusz.banaszczyk.app.gameRPS.GameAction;
import pl.sda.mateusz.banaszczyk.app.gameRPS.player.RandomPlayer;
import pl.sda.mateusz.banaszczyk.app.gameRPS.player.ScannerPlayer;

public class EnglishTranslation extends Translation {


    @Override
    public String cont(){
        return "Do you play again? y/n";
    }

    @Override
    public String action(){
        return "Choose action Rock/Paper/Scissors: r/p/s";
           }

    @Override
    public String nick() {
        return "Enter your nickname:";
    }

    @Override
    public String actionText1(GameAction action1, RandomPlayer player1) {
        return String.format("%s selected action 1: %s",player1.nick, getAction(action1));
    }

    @Override
    public String actionText2(GameAction action2, ScannerPlayer player2) {
        return String.format("%s selected action 2: %s",player2.nick2, getAction(action2));
    }

    @Override
    public String player1Win(RandomPlayer player1) {
        return player1.nick + " wins!";
    }

    @Override
    public String player2Win(ScannerPlayer player2) {

        return player2.nick2 + " WINS!";
    }

    @Override
    public String draw() {
        return "Draw!";
    }

    String getAction(GameAction action){
        switch (action){
            default:
            case SCISSORS:
                return "Scissors";
            case PAPER:
                return "Paper";
            case ROCK:
                return "Rock";
        }
    }
}
