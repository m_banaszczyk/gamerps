package pl.sda.mateusz.banaszczyk.app.gameRPS;

public enum GameAction {
    ROCK,
    PAPER,
    SCISSORS
}
