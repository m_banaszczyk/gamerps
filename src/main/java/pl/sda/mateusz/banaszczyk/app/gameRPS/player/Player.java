package pl.sda.mateusz.banaszczyk.app.gameRPS.player;

import pl.sda.mateusz.banaszczyk.app.gameRPS.GameAction;

public abstract class Player {

    public abstract GameAction chooseAction();
}
