package pl.sda.mateusz.banaszczyk.app.gameRPS.player;

import pl.sda.mateusz.banaszczyk.app.gameRPS.GameAction;
import pl.sda.mateusz.banaszczyk.app.gameRPS.GameResult;
import pl.sda.mateusz.banaszczyk.app.gameRPS.language.Translation;


import java.util.Scanner;

public class GameRockPaperScissors {
    public static Translation translation;
    private final RandomPlayer player1;
    private final ScannerPlayer player2;
    public static String cont;

    public GameRockPaperScissors(RandomPlayer player1, ScannerPlayer player2, Translation translation) {
        this.player1 = player1;
        this.player2 = player2;
        this.translation = translation;
    }

    public void startGame() {
        String ScannerPlayer = player2.chooseNick();
        do {
            GameAction action1 = player1.chooseAction();
            GameAction action2 = player2.chooseAction();
            System.out.println(translation.actionText1(action1, player1));
            System.out.println(translation.actionText2(action2, player2));

            GameResult result = checkResult(action1, action2);
            switch (result) {
                case PLAYER_1_WIN:
                    System.out.println(translation.player1Win(player1));
                    break;
                case PLAYER_2_WIN:
                    System.out.println(translation.player2Win(player2));
                    break;
                case DRAW:
                    System.out.println(translation.draw());
                    break;
                default:
                    System.out.println("Inny wynik / Other result");
            }

            System.out.println(translation.cont());
            Scanner scanner = new Scanner(System.in);
            cont = scanner.next();
        } while (cont.equals("t") || cont.equals("y") || !cont.equals("n"));
    }

    private GameResult checkResult(GameAction action1, GameAction action2) {

        if (action1 == null || action2 == null) {
            throw new IllegalStateException("Brak akcji! / Null actions!");
        }
//remis

        if (action1 == action2) {
            return GameResult.DRAW;
        }
        if ((action1 == GameAction.ROCK && action2 == GameAction.SCISSORS) ||
                (action1 == GameAction.PAPER && action2 == GameAction.ROCK) ||
                (action1 == GameAction.SCISSORS && action2 == GameAction.PAPER)) {
            return GameResult.PLAYER_1_WIN;
        }
        return GameResult.PLAYER_2_WIN;
    }

}



