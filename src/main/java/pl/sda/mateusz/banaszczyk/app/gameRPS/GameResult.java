package pl.sda.mateusz.banaszczyk.app.gameRPS;

public enum GameResult {
    PLAYER_1_WIN,
    PLAYER_2_WIN,
    DRAW
}
